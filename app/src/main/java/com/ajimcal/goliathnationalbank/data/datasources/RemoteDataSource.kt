package com.ajimcal.goliathnationalbank.data.datasources

import com.ajimcal.goliathnationalbank.data.extensions.toListOfRates
import com.ajimcal.goliathnationalbank.data.extensions.toListOfTransaction
import com.ajimcal.goliathnationalbank.data.remote.services.ApiService
import com.ajimcal.goliathnationalbank.domain.exceptions.ResponseException
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import okhttp3.ResponseBody
import org.json.JSONArray
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSource @Inject constructor(
    private val apiService: ApiService
): RemoteDataSourceInterface {

    override suspend fun getTransaction(): List<Transaction> {
        val response = apiService.getAllTransactions()

        return if (response.isSuccessful){
            try{
                JSONArray((response.body() as ResponseBody).string()).toListOfTransaction()
            } catch (exception: Exception){
                throw ResponseException()
            }
        }else{
            throw ResponseException()
        }
    }

    override suspend fun getRates(): List<Rate> {
        val response = apiService.getRates()

        return if (response.isSuccessful){
            try{
                JSONArray((response.body() as ResponseBody).string()).toListOfRates()
            } catch (exception: Exception){
                throw ResponseException()
            }
        }else{
            throw ResponseException()
        }
    }

}