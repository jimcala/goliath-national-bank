package com.ajimcal.goliathnationalbank.data.datasources

import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction

interface RemoteDataSourceInterface {

    suspend fun getTransaction(): List<Transaction>
    suspend fun getRates(): List<Rate>

}