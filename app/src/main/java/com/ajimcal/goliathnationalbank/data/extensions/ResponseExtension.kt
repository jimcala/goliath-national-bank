package com.ajimcal.goliathnationalbank.data.extensions

import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.utils.transformToInt
import org.json.JSONArray

fun JSONArray.toListOfTransaction(): List<Transaction>{
    val transactions = mutableListOf<Transaction>()
    for (i in 0 until this.length()) {
        val transaction = this.getJSONObject(i)

        val sku = transaction.getString("sku")
        val amount = transaction.getDouble("amount").transformToInt()
        val currency = enumValueOf<Currency>(transaction.getString("currency"))

        transactions.add(Transaction(sku, amount, currency))
    }
    return transactions
}

fun JSONArray.toListOfRates(): List<Rate>{
    val rates = mutableListOf<Rate>()
    for (i in 0 until this.length()) {
        val rate = this.getJSONObject(i)

        val from = enumValueOf<Currency>(rate.getString("from"))
        val to = enumValueOf<Currency>(rate.getString("to"))
        // Depends of number of decimals in rate (Nowadays 2 decimals)
        val rateConvert = rate.getDouble("rate").transformToInt()

        rates.add(Rate(from, to, rateConvert))
    }
    return rates
}