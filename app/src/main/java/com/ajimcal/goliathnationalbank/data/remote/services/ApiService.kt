package com.ajimcal.goliathnationalbank.data.remote.services

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiService {

    @Headers("Accept: application/json","Content-type:application/json")
    @GET("/transactions")
    suspend fun getAllTransactions(): Response<ResponseBody>

    @Headers("Accept: application/json","Content-type:application/json")
    @GET("/rates")
    suspend fun getRates(): Response<ResponseBody>

    companion object {
        private const val BASE_URL = "https://quiet-stone-2094.herokuapp.com"

        @JvmStatic
        fun create(): ApiService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

}