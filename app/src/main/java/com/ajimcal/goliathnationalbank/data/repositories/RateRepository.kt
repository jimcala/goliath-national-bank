package com.ajimcal.goliathnationalbank.data.repositories

import com.ajimcal.goliathnationalbank.data.datasources.RemoteDataSourceInterface
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction

class RateRepository(
    private val remoteDataSource: RemoteDataSourceInterface
) {

    suspend fun getRates(): List<Rate> {
        return remoteDataSource.getRates()
    }

}