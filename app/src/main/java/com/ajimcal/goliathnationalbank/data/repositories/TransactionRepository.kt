package com.ajimcal.goliathnationalbank.data.repositories

import com.ajimcal.goliathnationalbank.data.datasources.RemoteDataSourceInterface
import com.ajimcal.goliathnationalbank.domain.models.Transaction

class TransactionRepository(
    private val remoteDataSource: RemoteDataSourceInterface
) {

    suspend fun getTransaction(): List<Transaction> {
        return remoteDataSource.getTransaction()
    }

}