package com.ajimcal.goliathnationalbank.di

import com.ajimcal.goliathnationalbank.data.remote.services.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    fun apiProvider(): ApiService = ApiService.create()

}