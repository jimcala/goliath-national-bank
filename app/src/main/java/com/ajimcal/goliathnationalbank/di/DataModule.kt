package com.ajimcal.goliathnationalbank.di

import com.ajimcal.goliathnationalbank.data.datasources.RemoteDataSource
import com.ajimcal.goliathnationalbank.data.datasources.RemoteDataSourceInterface
import com.ajimcal.goliathnationalbank.data.remote.services.ApiService
import com.ajimcal.goliathnationalbank.data.repositories.RateRepository
import com.ajimcal.goliathnationalbank.data.repositories.TransactionRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun dataSource(apiService: ApiService): RemoteDataSourceInterface {
        return RemoteDataSource(apiService)
    }

    @Provides
    fun transactionRepositoryProvider(remoteDataSource: RemoteDataSourceInterface): TransactionRepository{
        return TransactionRepository(remoteDataSource)
    }

    @Provides
    fun rateRepositoryProvider(remoteDataSource: RemoteDataSourceInterface): RateRepository{
        return RateRepository(remoteDataSource)
    }

}