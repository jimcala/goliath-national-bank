package com.ajimcal.goliathnationalbank.di

import com.ajimcal.goliathnationalbank.data.repositories.RateRepository
import com.ajimcal.goliathnationalbank.data.repositories.TransactionRepository
import com.ajimcal.goliathnationalbank.usecases.GetRatesUseCase
import com.ajimcal.goliathnationalbank.usecases.GetTransactionUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {

    @Provides
    fun getTransactionUseCase(repository: TransactionRepository): GetTransactionUseCase {
        return GetTransactionUseCase(repository)
    }

    @Provides
    fun getRatesUseCase(repository: RateRepository): GetRatesUseCase {
        return GetRatesUseCase(repository)
    }

}