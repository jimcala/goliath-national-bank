package com.ajimcal.goliathnationalbank.domain.constants

object CodeError {
    val ERROR_GENERIC = 1000
    val REQUEST_EXCEPTION = 1001
    val RESPONSE_EXCEPTION = 1002
    val NOT_FOUND = 1004
    val CONNECTION_EXCEPTION = 1005
}