package com.ajimcal.goliathnationalbank.domain.exceptions

class NotFoundException: Exception()