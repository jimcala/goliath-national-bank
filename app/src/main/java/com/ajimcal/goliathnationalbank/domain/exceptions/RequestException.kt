package com.ajimcal.goliathnationalbank.domain.exceptions

class RequestException: Exception()