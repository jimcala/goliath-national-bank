package com.ajimcal.goliathnationalbank.domain.models

enum class Currency(
    val symbol: String
){
    EUR("€"),
    USD("$"),
    AUD("A$"),
    CAD("C$")
}