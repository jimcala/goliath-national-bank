package com.ajimcal.goliathnationalbank.domain.models

data class Rate(
    val from: Currency,
    val to: Currency,
    val rate: Int
)
