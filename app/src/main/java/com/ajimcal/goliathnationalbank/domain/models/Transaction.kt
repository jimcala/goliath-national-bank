package com.ajimcal.goliathnationalbank.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Transaction(
    val sku: String,
    val amount: Int,
    val currency: Currency
) : Parcelable
