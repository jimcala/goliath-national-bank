package com.ajimcal.goliathnationalbank.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.utils.DataHolder
import com.ajimcal.goliathnationalbank.utils.calculateTotal
import kotlinx.coroutines.launch

class TransactionViewModel: ViewModel() {

    private val _getTotal = MutableLiveData<DataHolder<Pair<Int, Currency>>>()
    val getTotal: LiveData<DataHolder<Pair<Int, Currency>>> get() = _getTotal

    fun getTotal(transactions: List<Transaction>, rates: List<Rate>) {
        viewModelScope.launch {
            val currency = Currency.EUR
            val total = transactions.calculateTotal(rates.toMutableList(), currency)
            _getTotal.postValue(DataHolder.Success(Pair(total, currency)))
        }
    }
}