package com.ajimcal.goliathnationalbank.presentation.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajimcal.goliathnationalbank.databinding.ItemTransactionBinding
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.utils.toDecimal

class TransactionsAdapter: RecyclerView.Adapter<TransactionsAdapter.OperationViewHolder>()  {

    private var items: List<Transaction> = listOf()
    inner class OperationViewHolder(val binding: ItemTransactionBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionsAdapter.OperationViewHolder {
        val binding = ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OperationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TransactionsAdapter.OperationViewHolder, position: Int) {
        holder.binding.amount.text = items[position].amount.toDecimal()
        holder.binding.currency.text = items[position].currency.symbol
    }

    override fun getItemCount() = items.size

    fun updateData(data: List<Transaction> = listOf()) {
        items = data
        notifyDataSetChanged()
    }
}