package com.ajimcal.goliathnationalbank.presentation.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ajimcal.goliathnationalbank.R
import com.ajimcal.goliathnationalbank.databinding.FragmentDetailBinding
import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.presentation.products.ProductsViewModel
import com.ajimcal.goliathnationalbank.utils.DataHolder
import com.ajimcal.goliathnationalbank.utils.toDecimal
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionsFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TransactionViewModel by viewModels()
    private val viewModelProducts: ProductsViewModel by activityViewModels()

    private val adapter: TransactionsAdapter by lazy { TransactionsAdapter() }
    private val args: TransactionsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        binding.transactionList.layoutManager = LinearLayoutManager(requireContext())
        binding.transactionList.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        binding.transactionList.adapter = adapter

        showProgress(true)
        adapter.updateData(args.transactions.toList())
        binding.totalTransactions.text = getString(R.string.total, args.transactions.size)

        initObservers()
        viewModel.getTotal(args.transactions.toList(), viewModelProducts.getRates)

        return binding.root
    }

    private fun initObservers(){
        viewModel.getTotal.observe(viewLifecycleOwner, Observer { value ->
            manageData(value)
        })
    }

    private fun manageData(result: DataHolder<Pair<Int, Currency>>){
        when(result){
            is DataHolder.Loading -> {
                showProgress(true)
            }

            is DataHolder.Success -> {
                Log.d("TRANSACTIONS", "Total ${args.transactions.size} transactions: ${result.data.first.toDecimal()} ${result.data.second}")
                binding.totalAmount.text = "${result.data.first.toDecimal()} ${result.data.second.symbol}"
                showProgress(false)
            }

            is DataHolder.Fail -> {
                Log.d("PRODUCTS", "Error: ${result.error}")
                binding.emptyList.isVisible = true
                binding.emptyList.text = getString(R.string.error, result.error)
                showProgress(false)
            }
        }
    }

    private fun showProgress(show: Boolean) {
        binding.includeLoading.root.isVisible = show
    }

}