package com.ajimcal.goliathnationalbank.presentation.products

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajimcal.goliathnationalbank.R
import com.ajimcal.goliathnationalbank.databinding.ItemProductBinding
import com.ajimcal.goliathnationalbank.domain.models.Transaction


class ProductsAdapter constructor(
    private val functionOnClick: (transations: List<Transaction>) -> Unit
): RecyclerView.Adapter<ProductsAdapter.OperationViewHolder>()  {

    private var items: Map<String, List<Transaction>> = mapOf()
    inner class OperationViewHolder(val binding: ItemProductBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductsAdapter.OperationViewHolder {
        val binding = ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OperationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductsAdapter.OperationViewHolder, position: Int) {
        val productName = items.keys.elementAt(position)
        holder.binding.productName.text = productName
        holder.binding.totalTransactions.text = holder.itemView.context.getString(R.string.total_transactions, items.getValue(productName).size)
        holder.itemView.setOnClickListener{ functionOnClick(items.getValue(productName)) }
    }

    override fun getItemCount() = items.size

    fun updateData(data: Map<String, List<Transaction>> = mapOf()) {
        items = data
        notifyDataSetChanged()
    }
}