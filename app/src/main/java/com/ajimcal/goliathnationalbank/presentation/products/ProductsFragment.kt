package com.ajimcal.goliathnationalbank.presentation.products

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ajimcal.goliathnationalbank.R
import com.ajimcal.goliathnationalbank.databinding.FragmentProductsBinding
import com.ajimcal.goliathnationalbank.domain.constants.CodeError
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.utils.DataHolder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductsFragment : Fragment() {

    private var _binding: FragmentProductsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ProductsViewModel by activityViewModels()

    private val adapter: ProductsAdapter by lazy { ProductsAdapter{ goToDetail(it) } }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductsBinding.inflate(inflater, container, false)
        binding.productList.layoutManager = LinearLayoutManager(requireContext())
        binding.productList.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        binding.productList.adapter = adapter
        binding.rateButton.setOnClickListener {
            this.findNavController().navigate(ProductsFragmentDirections.actionProductsFragmentToRatesFragment())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        viewModel.getData()
    }

    private fun initObservers(){
        viewModel.getTransactions.observe(viewLifecycleOwner, Observer { value ->
            manageData(value)
        })
    }

    private fun manageData(result: DataHolder<Map<String, List<Transaction>>>){
        when(result){
            is DataHolder.Loading -> {
                showProgress(true)
            }

            is DataHolder.Success -> {
                Log.d("PRODUCTS", "Total products: ${result.data.size}")
                adapter.updateData(result.data)
                showProgress(false)
            }

            is DataHolder.Fail -> {
                Log.d("PRODUCTS", "Error: ${result.error}")
                binding.emptyList.isVisible = true
                when (result.error){
                    CodeError.NOT_FOUND -> binding.emptyList.text = getString(R.string.error, result.error)
                    CodeError.CONNECTION_EXCEPTION -> binding.emptyList.text = getString(R.string.error_connection)
                }
                binding.rateButton.isVisible = false
                showProgress(false)
            }
        }
    }

    private fun showProgress(show: Boolean) {
        binding.includeLoading.root.isVisible = show
    }

    private fun goToDetail(transactions: List<Transaction>){
        Log.d("PRODUCTS", "Click on the product ${transactions.first().sku}")
        this.findNavController().navigate(
            ProductsFragmentDirections.actionProductsFragmentToDetailFragment(
                transactions[0].sku,
                transactions.toTypedArray()
            )
        )
    }

}