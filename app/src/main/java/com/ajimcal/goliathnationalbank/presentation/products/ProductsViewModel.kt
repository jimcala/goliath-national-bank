package com.ajimcal.goliathnationalbank.presentation.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ajimcal.goliathnationalbank.domain.exceptions.NotFoundException
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.usecases.GetRatesUseCase
import com.ajimcal.goliathnationalbank.usecases.GetTransactionUseCase
import com.ajimcal.goliathnationalbank.utils.DataHolder
import com.ajimcal.goliathnationalbank.utils.toErrorCode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val getTransactionUseCase: GetTransactionUseCase,
    private val getRatesUseCase: GetRatesUseCase
): ViewModel() {

    private val _getTransactions = MutableLiveData<DataHolder<Map<String, List<Transaction>>>>()
    val getTransactions: LiveData<DataHolder<Map<String, List<Transaction>>>> get() = _getTransactions
    private var _rates = listOf<Rate>()
    val getRates: List<Rate> get() = _rates
    var mustRequest = true

    fun getData(){
        if (mustRequest) {
            _getTransactions.value = DataHolder.Loading()
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    val transactions = getTransactionUseCase.invoke().groupBy { it.sku }
                    _rates = getRatesUseCase.invoke()
                    if (transactions.isEmpty()) throw NotFoundException()
                    _getTransactions.postValue(DataHolder.Success(transactions))
                    mustRequest = false
                } catch (e: Exception) {
                    _getTransactions.postValue(DataHolder.Fail(e.toErrorCode()))
                }
            }
        }
    }

}