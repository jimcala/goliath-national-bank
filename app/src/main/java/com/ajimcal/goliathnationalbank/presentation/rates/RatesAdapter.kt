package com.ajimcal.goliathnationalbank.presentation.rates

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajimcal.goliathnationalbank.databinding.ItemRateBinding
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.utils.toDecimal

class RatesAdapter : RecyclerView.Adapter<RatesAdapter.RateViewHolder>() {

    private var items: List<Rate> = listOf()

    inner class RateViewHolder(val binding: ItemRateBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RatesAdapter.RateViewHolder {
        val binding = ItemRateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RateViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RatesAdapter.RateViewHolder, position: Int) {
        holder.binding.currencies.text = "${items[position].from} (${items[position].from.symbol}) \t ➜ \t ${items[position].to} (${items[position].to.symbol})"
        holder.binding.rate.text = items[position].rate.toDecimal()
    }

    override fun getItemCount() = items.size

    fun updateData(data: List<Rate> = listOf()) {
        items = data
        notifyDataSetChanged()
    }
}