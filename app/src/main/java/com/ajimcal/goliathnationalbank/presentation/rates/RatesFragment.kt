package com.ajimcal.goliathnationalbank.presentation.rates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ajimcal.goliathnationalbank.databinding.FragmentRatesBinding
import com.ajimcal.goliathnationalbank.presentation.products.ProductsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RatesFragment : Fragment() {

    private var _binding: FragmentRatesBinding? = null
    private val binding get() = _binding!!
    private val viewModelProducts: ProductsViewModel by activityViewModels()
    private val adapter: RatesAdapter by lazy { RatesAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentRatesBinding.inflate(inflater, container, false)
        binding.rateList.layoutManager = LinearLayoutManager(requireContext())
        binding.rateList.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        binding.rateList.adapter = adapter
        adapter.updateData(viewModelProducts.getRates)
        return binding.root
    }

}