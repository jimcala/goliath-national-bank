package com.ajimcal.goliathnationalbank.usecases

import com.ajimcal.goliathnationalbank.data.repositories.RateRepository
import com.ajimcal.goliathnationalbank.data.repositories.TransactionRepository
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction

class GetRatesUseCase(
    private val rateRepository: RateRepository
) {

    suspend fun invoke(): List<Rate>{
        return rateRepository.getRates()
    }

}