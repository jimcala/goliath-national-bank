package com.ajimcal.goliathnationalbank.usecases

import com.ajimcal.goliathnationalbank.data.repositories.TransactionRepository
import com.ajimcal.goliathnationalbank.domain.models.Transaction

class GetTransactionUseCase(
    private val transactionRepository: TransactionRepository
) {

    suspend fun invoke(): List<Transaction>{
        return transactionRepository.getTransaction()
    }

}