package com.ajimcal.goliathnationalbank.utils

sealed class DataHolder<out T: Any> {

    data class Success<out T: Any>(val data: T): DataHolder<T>()

    data class Fail(val error: Int): DataHolder<Nothing>()

    data class Loading(val show: Boolean? = null): DataHolder<Nothing>()
}