package com.ajimcal.goliathnationalbank.utils

import kotlin.math.roundToInt

fun Double.transformToInt(): Int{
    return (this * 100).roundToInt()
}