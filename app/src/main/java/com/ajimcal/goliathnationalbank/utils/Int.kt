package com.ajimcal.goliathnationalbank.utils

import com.ajimcal.goliathnationalbank.domain.models.Rate
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.pow

fun Int.toDecimal(): String{
    return this.toDouble().div(100).toString()
}

fun Long.roundHalfToEven(base10Power: Int): Int{
    return BigDecimal
        .valueOf(this.div(10.0.pow(base10Power)))
        .setScale(2, RoundingMode.HALF_EVEN)
        .movePointRight(2)
        .toInt()
}

fun Int.getConversion(rates: List<Rate>): Int{
    var conversionRate = 1
    rates.forEach { conversionRate *= it.rate }
    return (this * conversionRate.toLong()).roundHalfToEven(rates.size*2+2)
}