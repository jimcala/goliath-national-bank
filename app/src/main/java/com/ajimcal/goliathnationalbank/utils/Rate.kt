package com.ajimcal.goliathnationalbank.utils

import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate

fun List<Rate>.getConversionRates(
    from: Currency,
    to: Currency,
    previousFrom: Currency? = null,
    ratesAccumulator: MutableList<Rate> = mutableListOf()
): MutableList<Rate> {
    val directRate = this.firstOrNull { it.from == from && it.to == to }
    return directRate?.let {
        ratesAccumulator.add(directRate)
        ratesAccumulator
    } ?: run {
        val indirectRate = this.first { it.from == from && it.to != previousFrom }
        ratesAccumulator.add(indirectRate)
        this.getConversionRates(indirectRate.to, to, indirectRate.from, ratesAccumulator)
    }
}