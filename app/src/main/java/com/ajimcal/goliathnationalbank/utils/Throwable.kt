package com.ajimcal.goliathnationalbank.utils

import com.ajimcal.goliathnationalbank.domain.constants.CodeError.CONNECTION_EXCEPTION
import com.ajimcal.goliathnationalbank.domain.constants.CodeError.ERROR_GENERIC
import com.ajimcal.goliathnationalbank.domain.constants.CodeError.NOT_FOUND
import com.ajimcal.goliathnationalbank.domain.constants.CodeError.REQUEST_EXCEPTION
import com.ajimcal.goliathnationalbank.domain.constants.CodeError.RESPONSE_EXCEPTION
import com.ajimcal.goliathnationalbank.domain.exceptions.NotFoundException
import com.ajimcal.goliathnationalbank.domain.exceptions.RequestException
import com.ajimcal.goliathnationalbank.domain.exceptions.ResponseException
import java.net.UnknownHostException

fun Throwable.toErrorCode() = when(this){
    is RequestException -> REQUEST_EXCEPTION
    is ResponseException -> RESPONSE_EXCEPTION
    is NotFoundException -> NOT_FOUND
    is UnknownHostException -> CONNECTION_EXCEPTION
    else -> {
        this.printStackTrace()
        ERROR_GENERIC
    }
}