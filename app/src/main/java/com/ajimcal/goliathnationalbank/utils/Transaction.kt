package com.ajimcal.goliathnationalbank.utils

import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction

fun List<Transaction>.calculateTotal(rates: MutableList<Rate>, toCurrency: Currency): Int{
    var total = 0
    this.forEach {
        val amount = when(it.currency != toCurrency){
            true -> it.amount.getConversion(rates.getConversionRates(it.currency, toCurrency))
            false -> it.amount
        }
        total += amount
    }
    return total
}