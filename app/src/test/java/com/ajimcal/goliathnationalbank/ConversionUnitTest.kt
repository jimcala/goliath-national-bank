package com.ajimcal.goliathnationalbank

import com.ajimcal.goliathnationalbank.data.extensions.toListOfRates
import com.ajimcal.goliathnationalbank.data.extensions.toListOfTransaction
import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.utils.getConversion
import com.ajimcal.goliathnationalbank.utils.getConversionRates
import org.json.JSONArray
import org.junit.Assert.assertEquals
import org.junit.Test

class ConversionUnitTest {

    private val jsonRates = ClassLoader.getSystemResource("rates.json").readText()
    private val rates: List<Rate> by lazy { JSONArray(jsonRates).toListOfRates() }

    private val jsonTransaction = ClassLoader.getSystemResource("transactions.json").readText()
    private val transactions: List<Transaction> by lazy { JSONArray(jsonTransaction).toListOfTransaction() }

    @Test
    fun round_half_to_even_up(){
        val transaction = transactions[0]
        val toCurrency = Currency.EUR
        val result = transaction.amount.getConversion(rates.getConversionRates(transaction.currency, toCurrency))
        val expected = 2344
        assertEquals(expected, result)
    }

    @Test
    fun round_half_to_even_middle_up(){
        val transaction = transactions[1]
        val toCurrency = Currency.EUR
        val result = transaction.amount.getConversion(rates.getConversionRates(transaction.currency, toCurrency))
        val expected = 1482
        assertEquals(result, expected)
    }

    @Test
    fun round_half_to_even_down(){
        val transaction = transactions[2]
        val toCurrency = Currency.EUR
        val result = transaction.amount.getConversion(rates.getConversionRates(transaction.currency, toCurrency))
        val expected = 5999
        assertEquals(expected, result)
    }

    @Test
    fun round_half_to_even_middle_down(){
        val transaction = transactions[3]
        val toCurrency = Currency.EUR
        val result = transaction.amount.getConversion(rates.getConversionRates(transaction.currency, toCurrency))
        val expected = 450
        assertEquals(expected, result)
    }

    @Test
    fun round_half_to_even_small_amount(){
        val transaction = transactions[4]
        val toCurrency = Currency.EUR
        val result = transaction.amount.getConversion(rates.getConversionRates(transaction.currency, toCurrency))
        val expected = 30
        assertEquals(expected, result)
    }

    @Test
    fun round_half_to_even_big_amount(){
        val transaction = transactions[5]
        val toCurrency = Currency.EUR
        val result = transaction.amount.getConversion(rates.getConversionRates(transaction.currency, toCurrency))
        val expected = 1036411
        assertEquals(expected, result)
    }

}