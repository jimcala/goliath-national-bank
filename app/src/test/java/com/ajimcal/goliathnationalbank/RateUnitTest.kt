package com.ajimcal.goliathnationalbank

import com.ajimcal.goliathnationalbank.data.extensions.toListOfRates
import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.utils.getConversionRates
import com.ajimcal.goliathnationalbank.utils.transformToInt
import org.json.JSONArray
import org.junit.Test

class RateUnitTest {

    private val jsonRates = ClassLoader.getSystemResource("rates.json").readText()
    private val rates: List<Rate> by lazy { JSONArray(jsonRates).toListOfRates() }

    @Test
    // AUD -> EUR
    fun direct_conversion_rate_AUD_to_EUR() {
        val conversionRates = rates.getConversionRates(Currency.AUD, Currency.EUR)
        assert(conversionRates[0] == Rate(Currency.AUD, Currency.EUR, 0.85.transformToInt()))
    }

    @Test
    // AUD -> EUR
    fun direct_conversion_rate_USD_to_CAD() {
        val conversionRates = rates.getConversionRates(Currency.USD, Currency.CAD)
        assert(conversionRates[0] == Rate(Currency.USD, Currency.CAD, 0.72.transformToInt()))
    }

    @Test
    // USD -> AUD -> EUR
    fun indirect_conversion_rate_USD_to_EUR() {
        val conversionRates = rates.getConversionRates(Currency.USD, Currency.EUR)
        assert(conversionRates[0] == Rate(Currency.USD, Currency.AUD, 0.83.transformToInt()))
        assert(conversionRates[1] == Rate(Currency.AUD, Currency.EUR, 0.85.transformToInt()))
    }

    @Test
    // CAD -> USD -> AUD -> EUR
    fun indirect_conversion_rate_CAD_to_EUR() {
        val conversionRates = rates.getConversionRates(Currency.CAD, Currency.EUR)
        assert(conversionRates[0] == Rate(Currency.CAD, Currency.USD, 1.39.transformToInt()))
        assert(conversionRates[1] == Rate(Currency.USD, Currency.AUD, 0.83.transformToInt()))
        assert(conversionRates[2] == Rate(Currency.AUD, Currency.EUR, 0.85.transformToInt()))
    }
}