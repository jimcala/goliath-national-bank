package com.ajimcal.goliathnationalbank

import com.ajimcal.goliathnationalbank.data.extensions.toListOfRates
import com.ajimcal.goliathnationalbank.data.extensions.toListOfTransaction
import com.ajimcal.goliathnationalbank.domain.models.Currency
import com.ajimcal.goliathnationalbank.domain.models.Rate
import com.ajimcal.goliathnationalbank.domain.models.Transaction
import com.ajimcal.goliathnationalbank.utils.calculateTotal
import org.json.JSONArray
import org.junit.Assert.assertEquals
import org.junit.Test

class TotalUnitTest {

    private val jsonRates = ClassLoader.getSystemResource("rates.json").readText()
    private val rates: List<Rate> by lazy { JSONArray(jsonRates).toListOfRates() }

    private val jsonTransaction = ClassLoader.getSystemResource("transactions.json").readText()
    private val transactions: List<Transaction> by lazy { JSONArray(jsonTransaction).toListOfTransaction() }

    @Test
    fun calculate_total(){
        val sku = "H8273"
        val currency = Currency.EUR
        val result = transactions.filter { it.sku == sku }.calculateTotal(rates.toMutableList(), currency)
        /**
         * 31.3 USD -> 22.08 EUR
         * 33.5 USD -> 23.63 EUR
         * 25.4 AUD -> 21.59 EUR
         * 16.4 CAD -> 16.08 EUR
         *             26.50 EUR
         * 21.5 AUD -> 18.28 EUR
         * 24.4 USD -> 17.21 EUR
         * ---------------------
         *    TOTAL -> 145.37 EUR
         */
        val expected = 14537
        assertEquals(expected, result)
    }
}